#include <iostream>
#include <string>
#include <cmath>
using namespace std;

float GetDistance(float x1, float y1, float x2, float y2)
{
	float dx = x2 - x1;
	float dy = y2 - y1;

	float dxSq = pow(dx, 2);
	float dySq = pow(dy, 2);
	float sum = dxSq + dySq;
	float distance = sqrt(sum);
	return distance;
}

//Adding a comment here as instructed

int main()
{
	cout << "DISTANCE" << endl;

	while (true)
	{
		float x1, y1, x2, y2;

		cout << "1st coordinate pair, enter x and y: ";
		cin >> x1 >> y1;

		cout << "2nd coordinate pair, enter x and y: ";
		cin >> x2 >> y2;

		float distance = GetDistance(x1, y1, x2, y2);
		cout << "Distance: " << distance << endl;

		cout << endl << endl;
	}

	return 0;
}
